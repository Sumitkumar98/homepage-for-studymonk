const tl = new TimelineMax();

let deviceScreenHeight = window.innerHeight;
let scrolledScreenValue = 0;
let floatingIcon = document.querySelector('#floatingButton');
let crossOfNav = document.querySelector('#crossOfNav');
let menuIcon = document.querySelector('#menuIcon');

// window.addEventListener('scroll', function(e) {
//     scrolledScreenValue = window.scrollY;
//     if (scrolledScreenValue > window.innerHeight) {
//         floatingIcon.style.display = 'flex';
//     } else {
//         floatingIcon.style.display = 'none';
//     }
// });

floatingIcon.addEventListener('click', function() {
    tl.fromTo('#halfscreennav', 1, {
        top: '-100vh'
    }, {
        top: '0%',
        ease: Expo.easeInOut
    })
})

menuIcon.addEventListener('click', function() {
    tl.fromTo('#halfscreennav', 1, {
        top: '-100vh'
    }, {
        top: '0%',
        ease: Expo.easeInOut
    })
})

crossOfNav.addEventListener('click', function() {
    tl.fromTo('#halfscreennav', 1, {
        top: '0%'
    }, {
        top: '-100vh',
        ease: Expo.easeInOut
    })
})



$(function() {
    setInterval(function() {
        $('#poly22').fadeOut(150).delay(2000).fadeIn(300).fadeOut(150).delay(1254);
        $('#poly23').fadeOut(150).delay(2000).fadeIn(300).fadeOut(150).delay(1254);
        $('#poly24').fadeOut(300).fadeIn(120).fadeOut(120).delay(1920);
        $('#poly25').fadeOut(150).delay(1200).fadeIn(300).fadeOut(150).delay(800);
        $('#poly26').fadeOut(700).fadeIn(300).fadeOut(160).delay(1350);
        $('#poly27').fadeOut(150).delay(2000).fadeIn(300).fadeOut(150).delay(1254);
        $('#poly28').fadeOut(300).fadeIn(120).fadeOut(120).delay(1920);
        $('#poly29').fadeOut(150).delay(2000).fadeIn(300).fadeOut(150).delay(1254);
        $('#poly30').fadeOut(300).fadeIn(120).fadeOut(120).delay(1920);
        $('#poly31').fadeOut(150).delay(2000).fadeIn(300).fadeOut(150).delay(1254);
        $('#poly30').fadeOut(300).fadeIn(120).fadeOut(120).delay(1920);
    }, 1);
});